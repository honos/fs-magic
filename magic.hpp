/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include <string>

namespace fs
{
namespace mime
{

/**
* Vrati priponu souboru
*
* @param    filename Uplna cesta k, nebo nazev, souboru
*
* @return   std::string Pripona souboru, nebo prazdny retezec pokud soubor postrada priponu
*/
std::string mime_get_file_extension(std::string filename);

/**
* Vrati retezec reprezentujici mime typ souboru
*
* @see      libmagic
*
* @param    filename Uplna cesta k souboru
*
* @return   std::string Retezec reprezentujici mime typ
*/
std::string mime_type(std::string filename);

}
}
