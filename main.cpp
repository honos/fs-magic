#include "magic.hpp"
#include "simple_types.hpp"

#include <stdio.h>

int main()
{
    printf("magic string: '%s'\n", fs::mime::mime_type("./main.cpp").c_str());
    printf("magic constant: '%i'\n", fs::mime::getType(fs::mime::mime_type("./main.cpp")));
    return 0;
}
