/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include <string>
#include <map>
namespace fs
{
namespace mime
{

enum SIMPLY_TYPE
{
    ARCHIVE     = 0,
    AUDIO       = 1,
    BITMAP      = 2,
    DRAWING     = 3,
    EXECUTABLE  = 4,
    MULTIMEDIA  = 5,
    OFFICE      = 6,
    OTHER       = 7,
    TEXT        = 8,
    VIDEO       = 9
};


/**
* Vrati ciselnou konstantu jednoducheho zarazeni magickeho typu
*
* @see      fs::mime::SIMPLY_TYPE
*
* @param    mime retezec reprezentujici mime typ
*
* @return   int Konstantu odpovidajici fs::mime::SIMPLY_TYPE
*/
int getType(std::string mime);

}
}
