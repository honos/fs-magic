/*
This file is a part of simple_types.hpp and fs::magic (filesystem::magic mime) library
*/

#include "simple_types.hpp"

namespace fs
{
namespace mime
{

/**
* Asociativni pole magickych typu ja jejich jednoducheho zarazeni
*/
std::map<std::string, int> simple_types =
{

// BITMAPS
    {std::string("image/gif"), SIMPLY_TYPE::BITMAP},
    {std::string("image/png"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-png"), SIMPLY_TYPE::BITMAP},
    {std::string("image/ief"), SIMPLY_TYPE::BITMAP},
    {std::string("image/jpeg"), SIMPLY_TYPE::BITMAP},
    {std::string("image/pjpeg"), SIMPLY_TYPE::BITMAP},
    {std::string("image/jp2"), SIMPLY_TYPE::BITMAP},
    {std::string("image/xbm"), SIMPLY_TYPE::BITMAP},
    {std::string("image/tiff"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-icon"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-ico"), SIMPLY_TYPE::BITMAP},
    {std::string("image/vnd.microsoft.icon"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-rgb"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-portable-pixmap"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-portable-graymap"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-portable-greymap"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-bmp"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-ms-bmp"), SIMPLY_TYPE::BITMAP},
    {std::string("image/bmp"), SIMPLY_TYPE::BITMAP},
    {std::string("application/x-bmp"), SIMPLY_TYPE::BITMAP},
    {std::string("application/bmp"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-photoshop"), SIMPLY_TYPE::BITMAP},
    {std::string("image/psd"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-psd"), SIMPLY_TYPE::BITMAP},
    {std::string("image/photoshop"), SIMPLY_TYPE::BITMAP},
    {std::string("image/vnd.adobe.photoshop"), SIMPLY_TYPE::BITMAP},
    {std::string("image/vnd.djvu"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x.djvu"), SIMPLY_TYPE::BITMAP},
    {std::string("image/x-djvu"), SIMPLY_TYPE::BITMAP},
    {std::string("image/webp"), SIMPLY_TYPE::BITMAP},

// DRAWINGS
    {std::string("image/svg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/svg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/svg+xml"), SIMPLY_TYPE::DRAWING},
    {std::string("image/svg+xml"), SIMPLY_TYPE::DRAWING},
    {std::string("application/postscript"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-latex"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-tex"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-dia-diagram"), SIMPLY_TYPE::DRAWING},
    {std::string("chemical/x-mdl-rgfile"), SIMPLY_TYPE::DRAWING},
    {std::string("chemical/x-mdl-rdfile"), SIMPLY_TYPE::DRAWING},
    {std::string("chemical/x-mdl-rxnfile"), SIMPLY_TYPE::DRAWING},
    {std::string("chemical/x-mdl-sdfile"), SIMPLY_TYPE::DRAWING},
    {std::string("chemical/x-mdl-molfile"), SIMPLY_TYPE::DRAWING},
    {std::string("drawing/dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("image/vnd.dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-autocad"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("image/x-dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/autocad_dwg"), SIMPLY_TYPE::DRAWING},
    {std::string("application/x-acad"), SIMPLY_TYPE::DRAWING},
    {std::string("application/acad"), SIMPLY_TYPE::DRAWING},

// AUDIOS
    {std::string("audio/mpeg3"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/mp3"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/mpeg"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/mp4"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/wave"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-wav"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/wav"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/mid"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/midi"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/basic"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/ogg"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/opus"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-aiff"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-pn-realaudio"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-realaudio"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/webm"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-matroska"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/x-flac"), SIMPLY_TYPE::AUDIO},
    {std::string("audio/flac"), SIMPLY_TYPE::AUDIO},

// VIDEOS
    {std::string("application/mpeg"), SIMPLY_TYPE::VIDEO},
    {std::string("video/mpeg"), SIMPLY_TYPE::VIDEO},
    {std::string("video/ogg"), SIMPLY_TYPE::VIDEO},
    {std::string("video/x-sgi-video"), SIMPLY_TYPE::VIDEO},
    {std::string("video/x-flv"), SIMPLY_TYPE::VIDEO},
    {std::string("video/webm"), SIMPLY_TYPE::VIDEO},
    {std::string("video/x-matroska"), SIMPLY_TYPE::VIDEO},
    {std::string("video/mp4"), SIMPLY_TYPE::VIDEO},

// MULTIMEDIA
    {std::string("video/x-ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("video/ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("audio/x-ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("audio/ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("application/x-ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("application/ogg"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("application/x-shockwave-flash"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("audio/x-pn-realaudio-plugin"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("model/iges"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("model/mesh"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("model/vrml"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("video/quicktime"), SIMPLY_TYPE::MULTIMEDIA},
    {std::string("video/x-msvideo"), SIMPLY_TYPE::MULTIMEDIA},

// TEXTS
    {std::string("text/plain"), SIMPLY_TYPE::TEXT},
    {std::string("text/html"), SIMPLY_TYPE::TEXT},
    {std::string("application/xhtml+xml"), SIMPLY_TYPE::TEXT},
    {std::string("text/xml"), SIMPLY_TYPE::TEXT},
    {std::string("application/xml"), SIMPLY_TYPE::TEXT},
    {std::string("text"), SIMPLY_TYPE::TEXT},
    {std::string("application/json"), SIMPLY_TYPE::TEXT},
    {std::string("text/csv"), SIMPLY_TYPE::TEXT},
    {std::string("text/tab-separated-values"), SIMPLY_TYPE::TEXT},

// ARCHIVES
    {std::string("application/x-zip"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/zip"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-gzip"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-bzip"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-bzip2"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-tar"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-stuffit"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-opc+zip"), SIMPLY_TYPE::ARCHIVE},
    {std::string("application/x-7z-compressed"), SIMPLY_TYPE::ARCHIVE},

// EXECUTABLES
    {std::string("application/x-python"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-perl"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-tcl"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-tcsh"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-csh"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-sh"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-bash"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("text/ecmascript"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-ecmascript"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-javascript"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("text/javascript"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/javascript"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-executable"), SIMPLY_TYPE::EXECUTABLE},
    {std::string("application/x-msdos-program"), SIMPLY_TYPE::EXECUTABLE},

// OFFICES
    {std::string("application/pdf application/acrobat"), SIMPLY_TYPE::OFFICE},
    {std::string("application/msword"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-excel"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-powerpoint"), SIMPLY_TYPE::OFFICE},
    {std::string("application/x-director"), SIMPLY_TYPE::OFFICE},
    {std::string("text/rtf"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.wordprocessingml.document"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.wordprocessingml.template"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-word.document.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-word.template.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.presentationml.template"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.presentationml.slideshow"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.presentationml.presentation"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-powerpoint.addin.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-powerpoint.presentation.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-powerpoint.presentation.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-powerpoint.slideshow.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.openxmlformats-officedocument.spreadsheetml.template"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-excel.sheet.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-excel.template.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-excel.addin.macroEnabled.12"), SIMPLY_TYPE::OFFICE},
    {std::string("application/vnd.ms-excel.sheet.binary.macroEnabled.12"), SIMPLY_TYPE::OFFICE}
};

int getType(std::string mime)
{
    std::map<std::string, int>:: iterator it = simple_types.find(mime);
    if(it != simple_types.end())
    {
        return it->second;
    }
    else
    {
        return SIMPLY_TYPE::OTHER;
    }

}

}
}
