/*
This file is a part of magic.hpp and fs::magic (filesystem::magic mime) library
*/

#include "magic.hpp"

#include "extensions.hpp"
#include "types.hpp"

#include <magic.h>

namespace fs
{
namespace mime
{

std::string mime_get_file_extension(std::string filename)
{
    std::string::size_type idx;
    idx = filename.rfind('.');

    if(idx != std::string::npos)
    {
        return filename.substr(idx+1);
    }
    else
    {
        return std::string("");
    }
}

std::string mime_type(std::string filename)
{
    int i;
    std::string file_ext = mime_get_file_extension(filename);
    std::string mime;
    if(file_ext.empty())
    {
        struct magic_set *magic = magic_open(MAGIC_ERROR/*|MAGIC_DEBUG*/|MAGIC_MIME_TYPE|MAGIC_CHECK);
        magic_load(magic,NULL);
        mime.assign(magic_file(magic,filename.c_str()));
        magic_close(magic);
    }
    else
    {
        for(i = 0; i < NUM_EXTENSIONS; i++)
        {
            if(file_ext.compare(mime_extensions[i]) == 0)
            {
                mime.assign(mime_types[i]);
            }
        }
    }

    if(mime.empty())
    {
        struct magic_set *magic = magic_open(MAGIC_ERROR|MAGIC_MIME|MAGIC_CHECK);
        magic_load(magic,NULL);
        mime.assign(magic_file(magic,filename.c_str()));
        magic_close(magic);
    }
    return mime;
}

}
}
